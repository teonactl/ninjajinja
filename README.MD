# Ninja Jinja v.0.2
## Bash script to format html files in jinja style

### Usage
cd ninjajinja
./ninja.sh [PATH/TO/FILE] [TAG] [ATTR] [JINJA_DIRECTORY]

This sed regex can help you to manage jinja's style attributes on html tags

Before to run this script, please take note of:

1. The path to the HTML file you want to change
2. The html tag that contains the attribute you want to change
3. The attribute you want to change
4. The name of your directory in your Jinja project

5. (OPTIONAL) if you want to enable batch mode [ -b --batch] , in this case your first argument should be a valid directory containing .html files



The regex automatically excludes paths like ('#') or external resources ( 'http', 'https', etc..)


NOTE: the arguments are positional, so give them in the correct sequence

This script will search your HTML in [PATH/TO/FILE], looks for the specidied [TAG] 
and changes the attribute [ATTR] you specified to a jinja renderable link pointing to
your project's folder.

#### EG.:

<[TAG] [OTHER_ATTRS] [ATTR]="[ORIGINAL_TEMPLATE_DIR]" [OTHER_ATTRS]></[TAG]>

To:

<[TAG] [OTHER_ATTRS] [ATTR]="{{ url_for('[JINJA_DIR]', filename='[YOUR_TEMPLATE_DIR]') }}" [OTHERS_ATTRS]> </[TAG]>


# Rendergen v.0.1

## Bash script for generate app.routes and flask render_template for each .html page in the specified folder


### Usage

/path/to/rendergen.sh [PATH/TO/TEMPLATE/FOLDER]

#### EG.:

    @app.route('/page1.html')
    def page1html():
        return render_template('page1.html')


    @app.route('/page2.html')
    def page2html():
        return render_template('page2.html')


    @app.route('/page3.html')
    def page3html():
        return render_template('page3.html')

