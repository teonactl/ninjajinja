#!/bin/bash

for filename in $1*.html ; do
	FILEJINJA=$(echo $filename | sed 's|\([^*]*\)/\([^*]*\)|\2|g')
	FUNC=$(echo $FILEJINJA | sed -E 's|\.+||;s|\-+||g' )
	

	echo -e "@app.route('/"$FILEJINJA"')\ndef "$FUNC"():\n\treturn render_template('"$FILEJINJA"')\n\n"

done
    