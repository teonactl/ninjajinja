#/bin/bash



if [ ! $1 ]
	then
		echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
		echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
		echo ""
		echo "Welcome, this regex can help you to manage jinja's style attributes on html tags"
		echo "Before to run this script, please take note of:"
		echo ""
		echo "1-The path to the file you want to change"
		echo "2-The html tag that contains the attribute you want to change"
		echo "3-The attribute you want to change"
		echo "4-The name of your directory in your Jinja project"
		echo ""
		echo "5- [OPTIONAL] -b or --batch if you want to convert all the .html files in the "
		echo "given directory."
		echo ""
		echo "NOTE: the arguments are positional, so give them in the correct sequence"
		echo "This script will change the attribute you specified to a jinja renderable link"
		echo "like: [ATTR]='{{ url_for('[JINJA_DIR]', filename='[YOUR_TEMPLATE_DIR]') }}'"
		echo ""		
		echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
		echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
		echo ""
		echo "Usage: ./ninja.sh [PATH/TO/FILE] [TAG] [ATTR] [JINJA_DIRECTORY] [BATCH_MODE]"
		echo ""
		echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
		echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
		exit
fi

ninjify () {
    if [ $3 = 'style' ]

		then
            #la regex scompone ogni riga del file in 5 blocchi e li ricompone aggiungendo le modifiche necessarie per essere compatibile con Flask/Jinja2.##
            #sostituisci questo#########################################con questo#################################################
            #######blocco1##tag##blocco2#attributo#blocco3#blocco4#);"#blocco5##blocco1#tag#attributo_e_jinjaurl#directory#blocco4#blocco2#blocco5#global(piu volte)
			sed 's|\([^*]*\)<'$2'\([^*]*\) '$3'=\([^*]*\)(\([^#*]*\))"\([^*]*\)|\1<'$2' '$3'="background:url({{"'$4'","\4"}})"\2\5|g' $1 | grep "<$2 "
		else
			
			sed 's|\([^*]*\)<'$2'\([^*]*\) '$3'="\([^#:\ *]*\)"\([^*]*\)|\1<'$2' '$3'="{{ url_for("'$4'", filename="\3")}}"\2\4|g' $1 | grep "<$2 "
	fi

	echo ""
	echo "" 
	echo "Do you confirm changes? [y/n]"
	read y
	if [ $y == 'y' ]
		then

			if [ $3 = 'style' ]

				then
					sed -i 's|\([^*]*\)<'$2'\([^*]*\) '$3'=\([^*]*\)(\([^#*]*\))"\([^*]*\)|\1<'$2' '$3'="background:url({{"'$4'","\4"}})"\2\5|g' $1
				
				else
				    sed -i 's|\([^*]*\)<'$2'\([^*]*\) '$3'="\([^#:\ *]*\)"\([^*]*\)|\1<'$2' '$3'="{{ url_for("'$4'", filename="\3")}}"\2\4|g' $1
				    
			fi
			echo ""
			echo "File changed!"
			
			
		else
		    
		    exit

    fi
}


if [ ! $5 ]
	
	then

		ninjify $1 $2 $3 $4

	else
		for filename in $1*.html ; do
			echo $filename
			ninjify $filename $2 $3 $4 
		done
fi